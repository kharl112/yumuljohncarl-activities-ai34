<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\BorrowedBooksController;
use App\Http\Controllers\PatronsController;
use App\Http\Controllers\ReturnedBooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('books')->group(function (){
    Route::post('add', [BooksController::class, "store"]);
    Route::put('update', [BooksController::class, "update"]);
    Route::delete('delete/id={id}', [BooksController::class, "destroy"]);
    Route::get('', [BooksController::class, "show"]);
    Route::get('limit={limit}', [BooksController::class, "limited"]);
    Route::get('id={id}', [BooksController::class, "info"]);
});

Route::prefix('patrons')->group(function (){
    Route::post('add', [PatronsController::class, "store"]);
    Route::put('update', [PatronsController::class, "update"]);
    Route::delete('delete/id={id}', [PatronsController::class, "destroy"]);
    Route::get('', [PatronsController::class, "show"]);
    Route::get('limit={limit}', [PatronsController::class, "limited"]);
    Route::get('id={id}', [PatronsController::class, "info"]);
});

Route::prefix('borrowed/books')->group(function (){
    Route::post('add', [BorrowedBooksController::class, "store"]);
    Route::get('', [BorrowedBooksController::class, "show"]);
    Route::get('limit={limit}', [BorrowedBooksController::class, "limited"]);
    Route::get('id={id}', [BorrowedBooksController::class, "info"]);
});

Route::prefix('returned/books')->group(function (){
    Route::post('add', [ReturnedBooksController::class, "store"]);
    Route::get('', [ReturnedBooksController::class, "show"]);
    Route::get('limit={limit}', [ReturnedBooksController::class, "limited"]);
    Route::get('id={id}', [ReturnedBooksController::class, "info"]);
});