<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::insert([[
            'category' => 'Science and Fiction'
        ], [
            'category' => 'Novel'
        ], [
            'category' => 'Educational'
        ], [
            'category' => 'Crime'
        ], [
            'category' => 'Biography'
        ]]);
    }
}
