<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function borrowed_books()
    {
        return $this->belongsToMany(Borrowed_Books::class, 'patron_id', 'id');
    }
    public function returned_books()
    {
        return $this->belongsToMany(Returned_Books::class, 'patron_id', 'id');
    }
}
