<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function categories()
    {
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }
    public function borrowed_books()
    {
        return $this->belongsToMany(Borrowed_Books::class, 'book_id', 'id');
    }
    public function returned_books()
    {
        return $this->belongsToMany(Returned_Books::class, 'book_id', 'id');
    }
}
