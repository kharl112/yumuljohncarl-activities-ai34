<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBooks extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function books()
    {
        return $this->hasMany(Books::class, 'id', 'book_id');
    }
    public function patrons()
    {
        return $this->hasMany(Patrons::class, 'id', 'patron_id');
    }
}
