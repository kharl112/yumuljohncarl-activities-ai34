<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnedBooksRequest;
use App\Models\Books;
use App\Models\BorrowedBooks;
use App\Models\ReturnedBooks;
use Illuminate\Http\Request;
use Throwable;

class ReturnedBooksController extends Controller
{
    public function store(ReturnedBooksRequest $request)
    {
        $request->validated();
        $req = json_decode(json_encode($request->input()));

        $borrowed_book_found = BorrowedBooks::where('book_id', $req->book_id)
            ->where('patron_id', $req->patron_id)
            ->first();

        $returned_book_found = ReturnedBooks::where('book_id', $req->book_id)
            ->where('patron_id', $req->patron_id)
            ->first();

        $book_found = Books::find($req->book_id);

        try {
            if (!empty($book_found)) {
                $book_found->copies += $req->copies;
                $book_found->save();
            }

            if ($borrowed_book_found->copies == $req->copies) {
                BorrowedBooks::find($borrowed_book_found->id)->delete();
            }

            $borrowed_book_found->copies -= $req->copies;
            $borrowed_book_found->save();

            if (empty($returned_book_found)) {
                $returned_book_found = new ReturnedBooks();
                $returned_book_found->book_id = $req->book_id;
                $returned_book_found->patron_id = $req->patron_id;
                $returned_book_found->copies = $req->copies;
            } else {
                $returned_book_found->copies += $req->copies;
            }
            $returned_book_found->save();
            return ["success" => ["message" => "book returned successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function show()
    {
        $returned = ReturnedBooks::with(['books', 'books.categories', 'patrons'])->get();
        return $returned;
    }

    public function limited($limit)
    {
        $returned = ReturnedBooks::with(['books', 'books.categories', 'patrons'])
            ->paginate($limit);

        return $returned;
    }

    public function info($id)
    {
        try {
            $returned = ReturnedBooks::with(['books', 'books.categories', 'patrons'])
                ->find($id);

            if (empty($returned)) {
                return response(["errors" => ["message" => "returned book not existed."]], 500);
            }

            return $returned;
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }
}
