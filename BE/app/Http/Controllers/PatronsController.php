<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatronsRequest;
use App\Models\Patrons;
use Illuminate\Http\Request;
use Throwable;

class PatronsController extends Controller
{
    public function store(PatronsRequest $request)
    {
        $request->validated();
        $req = json_decode(json_encode($request->input()));

        $patron = new Patrons();
        $patron->first_name = $req->first_name;
        $patron->last_name = $req->last_name;
        $patron->middle_name = $req->middle_name;
        $patron->email = $req->email;

        try {
            $patron->save();
            return ["success" => ["message" => "patron saved successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function update(PatronsRequest $request)
    {
        $request->validated();
        $req = json_decode(json_encode($request->input()));

        $patron = Patrons::find($req->id);
        $patron->first_name = $req->first_name;
        $patron->last_name = $req->last_name;
        $patron->middle_name = $req->middle_name;
        $patron->email = $req->email;

        try {
            $patron->save();
            return ["success" => ["message" => "patron updated successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Patrons::find($id)->delete();
            return ["success" => ["message" => "patron deleted successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function show()
    {
        $patrons = Patrons::orderBy('id')->get();
        return $patrons;
    }

    public function limited($limit)
    {
        $patrons = Patrons::orderBy('id')->paginate($limit);
        return $patrons;
    }

    public function info($id)
    {
        try {
            $patron = Patrons::find($id);
            if(empty($patron)){
                return response(["errors" => ["message" => "patron not existed."]], 500);
            }
            return $patron;
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }
}
