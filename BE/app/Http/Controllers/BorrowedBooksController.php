<?php

namespace App\Http\Controllers;

use App\Http\Requests\BorrowedBooksRequest;
use App\Models\Books;
use App\Models\BorrowedBooks;
use Illuminate\Http\Request;
use Throwable;

class BorrowedBooksController extends Controller
{
    public function store(BorrowedBooksRequest $request)
    {
        $request->validated();
        $req = json_decode(json_encode($request->input()));
        $book_found = Books::where('id', $req->book_id)->first();

        $borrow = BorrowedBooks::where('book_id', $req->book_id)
            ->where('patron_id', $req->patron_id)
            ->first();

        try {
            $book_found->copies -= $req->copies;
            $book_found->save();

            if (empty($borrow)) {
                $borrow = new BorrowedBooks();
                $borrow->patron_id = $req->patron_id;
                $borrow->book_id = $req->book_id;
                $borrow->copies = $req->copies;
            } else {
                $borrow->copies += $req->copies;
            }
            $borrow->save();
            return ["success" => ["message" => "book borrowed successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function show()
    {
        $borrowed = BorrowedBooks::with(['books', 'books.categories', 'patrons'])->get();
        return $borrowed;
    }

    public function limited($limit)
    {
        $borrowed = BorrowedBooks::with(['books', 'books.categories', 'patrons'])
            ->paginate($limit);

        return $borrowed;
    }

    public function info($id)
    {
        try {
            $borrowed = BorrowedBooks::with(['books', 'books.categories', 'patrons'])
                ->find($id);

            if (empty($borrowed)) {
                return response(["errors" => ["message" => "borrowed book not existed."]], 500);
            }

            return $borrowed;
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }
}
