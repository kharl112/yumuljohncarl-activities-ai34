<?php

namespace App\Http\Controllers;

use App\Http\Requests\BooksRequest;
use App\Models\Books;
use Illuminate\Http\Request;
use Throwable;

class BooksController extends Controller
{
    public function store(BooksRequest $request)
    {   
        $request->validated();
        $req = json_decode(json_encode($request->input()));

        $book = new Books();
        $book->name = $req->name;
        $book->author = $req->author;
        $book->copies = $req->copies;
        $book->category_id = $req->category_id;

        try {
            $book->save();
            return ["success" => ["message" => "book saved successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function update(BooksRequest $request)
    {
        $request->validated();
        $req = json_decode(json_encode($request->input()));

        $book = Books::find($req->id);
        $book->name = $req->name;
        $book->author = $req->author;
        $book->copies = $req->copies;
        $book->category_id = $req->category_id;

        try {
            $book->save();
            return ["success" => ["message" => "book updated successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function destroy($id)
    {
        try {
            Books::find($id)->delete();
            return ["success" => ["message" => "book deleted successfully!"]];
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }

    public function show()
    {
        $books = Books::with('categories')->get();
        return $books;
    }

    public function limited($limit)
    {
        $books = Books::with('categories')->paginate($limit);
        return $books;
    }

    public function info($id)
    {
        try {
            $book = Books::with('categories')->find($id);
            if(empty($book)){
                return response(["errors" => ["message" => "book not existed."]], 500);
            }
            return $book;
        } catch (Throwable $e) {
            return response(["errors" => ["unresolved" => "Something went wrong, please try again."]], 500);
        }
    }
}
