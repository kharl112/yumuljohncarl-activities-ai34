<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'max:255'],
            'author' => ['bail', 'required', 'max:255'],
            'copies' => ['bail', 'integer', 'gt:0', 'required'],
            'category_id' => ['integer', 'gt:0', 'lte:5', 'required']
        ];
    }
}
