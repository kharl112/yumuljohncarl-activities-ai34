<?php

namespace App\Http\Requests;

use App\Models\BorrowedBooks;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ReturnedBooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {
        $book = BorrowedBooks::where('book_id', $req->input('book_id'))
            ->where('patron_id', $req->input('patron_id'))
            ->first();

        if (empty($book)) {
            $copy = $req->input('copies');
        } else {
            $copy = $book->copies;
        }

        return [
            'book_id' => ['bail', 'integer', 'gt:0', 'required', 'exists:borrowed_books,book_id'],
            'patron_id' => ['bail', 'integer', 'gt:0', 'required', 'exists:borrowed_books,patron_id'],
            'copies' => ['bail', 'integer', 'gt:0', "lte:{$copy}", 'required'],
        ];
    }

    public function messages()
    {
        return [
            'book_id.exists' => ':attribute was not found from the borrowed books.',
            'patron_id.exists' => ':attribute was not found from the borrowed books.',
            'copies.lte' => 'the :attribute of books you want to return exceeds the amount you borrowed.',
        ];
    }
}
