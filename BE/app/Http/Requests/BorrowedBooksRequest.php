<?php

namespace App\Http\Requests;

use App\Models\Books;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class BorrowedBooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $req)
    {
        $book = Books::where('id', $req->input('book_id'))->first();
        if (empty($book)) {
            $copy = $req->input('copies');
        } else {
            $copy = $book->copies;
        }

        return [
            'book_id' => ['bail', 'integer', 'gt:0', 'required', 'exists:books,id'],
            'patron_id' => ['bail', 'integer', 'gt:0', 'required', 'exists:patrons,id'],
            'copies' => ['bail', 'integer', 'gt:0', "lte:{$copy}", 'required'],
        ];
    }

    public function messages()
    {
        return [
            'book_id.exists' => ':attribute was not found from the books available.',
            'patron_id.exists' => ':attribute was not found from the signed patrons.',
            'copies.lte' => 'the :attribute of books you have requested exceeds the amount available.',
        ];
    }
}
