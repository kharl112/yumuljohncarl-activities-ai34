const blackOut = () => {
    let data = document.getElementById('black-out');
    let bodyClass = document.getElementsByTagName('body');
    if(data.className === "black-out-hide"){
        bodyClass[0].style.overflowY = "hidden";
        data.className = "black-out-show";
        return data.innerHTML = `
            <div class = 'remove-index'>
                <p>Are you sure you want to remove this index?</p>
                <div class = 'btn-group'>
                    <button onclick = "blackOut();" class = 'btn-cancel' >Cancel</button>
                    <button onclick = "blackOut();" class = 'btn-ok' >Ok</button>
                </div>
            </div>`
     }
         bodyClass[0].style.overflowY = "auto";
         return data.className = "black-out-hide";
}