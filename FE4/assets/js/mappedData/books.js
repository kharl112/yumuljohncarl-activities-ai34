const books = () => {
    booksCon = document.getElementById("books-con");
    const data = [{
        type: "Novel",
        title: "In An Instant",
        author: "Suzanne Redfearn",
        copies: 5
    },
    {
        type: "Education",
        title: "First Days Of School",
        author: "Harry & Rosemary Wong",
        copies: 25
    },
    {
        type: "Novel",
        title: "The Buried  Book",
        author: "D.M. Pulley",
        copies: 7
    }];

    return data.map((search, index) => {
        return booksCon.innerHTML += `
            <div class = 'container-fluid book-box'>
                <div class = 'more-con'>
                    <div 
                        class="container-fluid dot-con" 
                        onclick="hideShow('moreBox${index}', 'more-box-show', 'more-box-hide')"
                    >
                        <div class = 'dots'></div>
                        <div class = 'dots'></div>
                        <div class = 'dots'></div>
                    </div>
                    <div class = 'more-box-hide' id = 'moreBox${index}'>
                        <p>Copies</p>
                        <button  class = 'more-btns btn-add'>ADD</button>
                        <button onclick = "blackOut(); hideShow('moreBox${index}', 'more-box-show', 'more-box-hide');" class = 'more-btns btn-del'>DELETE</button>
                        <button class = 'more-btns btn-ret'>RETURN</button>
                    </div>
                </div>
                <p class = 'book-type'>${search.type}</p>
                <p class = 'book-title'>${search.title}</p>
                <p class = 'book-author'>${search.author}</p>
                <p>
                    <strong>Available:</strong> x${search.copies}
                </p>
            </div>
        `
    })
}