const patrons = () => {
    patronCon = document.getElementById("patronCon");
    const data = [{
        name: "Kharl Yumul",
        email: "kharl123@example.com",
        date: "example date"
    },
    {
        name: "David Ross",
        email: "david68@example.com",
        date: "example date"
    },
    {
        name: "Another Guy",
        email: "someguy@example.com",
        date: "example date"
    }
]

    return data.map(search =>{
        return patronCon.innerHTML += `
            <div class = 'user-box'>
                <div class = 'editor'>
                    <img src = 'assets/img/editBtn.png' />
                    <img onclick = 'blackOut();' src = 'assets/img/delBtn.png' />
                </div>
                <div class = 'user'>
                <div class = 'circle-con'>
                        <div class = 'prof-circle'>
                            ${search.name[0]}
                        </div>
                </div>
                    <div class = 'user-info'>
                        <p class = 'user-name'>
                        ${search.name}
                        </p>

                        <p class='user-email'>
                        ${search.email}
                        </p>
                    </div>
                </div>
                <div class = 'date-con'>
                    ${search.date}
                </div>
            </div>
        `
    })
}