
const pie = () => {
    var ctx = document.getElementById('pie').getContext('2d');
    new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Fiction", "Thriller", "Mystery", "Narrative", "Poetry"],
            datasets: [{
                backgroundColor: ["#27AE60", "#2F80ED", "#9B51E0", "#EB5757", "#F2994A" ],
                data:[40, 10, 20, 25, 35],
                borderWidth: 0
            }]
        },
        options: {
            cutoutPercentage: 60,
            rotation: 2 * Math.PI,
            title: {
                display: true,
                position: 'top',
                fontSize: 15,
                text: 'Popular Book Genre',
                fontColor: '#48484D'
            },
            legend: {
                position: 'bottom',
                labels: {
                    fontSize: 13,
                    padding: 12,
                    fontFamily: "'Open Sans', sans-serif"
                }
            }
        }
    });
}