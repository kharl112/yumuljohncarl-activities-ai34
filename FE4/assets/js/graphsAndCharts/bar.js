
const bar = () => {
    var ctx = document.getElementById('bar').getContext('2d');
    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', "July"],
            datasets: [
                {
                    label: "Borrowed",
                    backgroundColor: "#005099",
                    data: [3, 7, 12, 19, 27, 11, 8]
                },
                {
                    label: "Returned",
                    backgroundColor: "#D8610B",
                    data: [4, 3, 7, 20, 29, 18, 11]
                }
                
            ]
        },
        options: {
            title: {
                text: "Borrowed And Returned Books",
                fontColor: "#D8610B",
                lineHeight: 2,
                fontSize: 13,
                position: "top",
                display: true
            },
            legend:{
                position: 'bottom'
            },
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            }

        }
    });
}