import Vue from "vue";
import Vuex from "vuex";
import { books } from "./modules/books";
import { patrons } from "./modules/patrons";
import { floaters } from "./modules/floaters";
import { graphs } from "./modules/graphs";
import { borrowed } from "./modules/borrowed";
import { returned } from "./modules/returned";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    books,
    patrons,
    floaters,
    graphs,
    borrowed,
    returned,
  },
});
