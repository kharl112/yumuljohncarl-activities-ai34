export const floaters = {
  state: () => ({
    add: false,
    del: false,
    borrow: false,
    return: false,
  }),
  getters: {
    viewAdd(state) {
      return state.add;
    },
    viewDel(state) {
      return state.del;
    },
    viewBorrow(state) {
      return state.borrow;
    },
    viewReturn(state) {
      return state.return;
    },
  },
  mutations: {
    showAdd(state) {
      return (state.add = !state.add);
    },
    showDel(state) {
      return (state.del = !state.del);
    },
    showBorrow(state) {
      return (state.borrow = !state.borrow);
    },
    showReturn(state) {
      return (state.return = !state.return);
    },
    unSub(state) {
      [state.del, state.add, state.borrow, state.return] = [
        false,
        false,
        false,
        false,
      ];
    },
  },
};
