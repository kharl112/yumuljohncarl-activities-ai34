import axios from "axios";
const { DEFAULT_API_URL } = require("../../../config");

export const borrowed = {
  state: () => ({
    borroweds: [],
  }),
  getters: {
    borrowedList: (state) => (book_id) => {
      let borrowers = [];
      borrowers = state.borroweds.filter(
        (borrowed) => borrowed.book_id === book_id
      );
      return borrowers;
    },
  },
  mutations: {
    setBorroweds(state, borroweds) {
      return (state.borroweds = borroweds);
    },
  },
  actions: {
    getBorroweds({ commit }) {
      axios.get(`${DEFAULT_API_URL}/borrowed/books`).then((response) => {
        commit("setBorroweds", response.data);
      });
    },
    addBorrow({ dispatch, commit }, info) {
      axios
        .post(`${DEFAULT_API_URL}/borrowed/books/add`, info)
        .then((response) => {
          commit("showBorrow", "", { root: true });
          dispatch("books/throwMessage", response.data.success, { root: true });
        })
        .catch((error) =>
          dispatch("books/throwMessage", error.response.data, { root: true })
        );
    },
  },
};
