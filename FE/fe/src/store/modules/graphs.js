import axios from "axios";
const { DEFAULT_API_URL } = require("../../../config");

export const graphs = {
  namespaced: true,
  state: () => ({
    dataBorrowed: [],
    labelsBorrowed: [],
    dataReturned: [],
    labelsReturned: [],
    flag: false,
  }),

  getters: {
    showLabelsBorrowed(state) {
      return state.labelsBorrowed;
    },
    showDataBorrowed(state) {
      return state.dataBorrowed;
    },
    showDataReturned(state) {
      return state.dataReturned;
    },
    showAll(state) {
      let returned = state.dataReturned;
      let borrowed = state.dataBorrowed;
      return ([
        returned[0] + borrowed[0],
        returned[1] + borrowed[1],
        returned[2] + borrowed[2],
        returned[3] + borrowed[3],
        returned[4] + borrowed[4]
      ]);
    },
    getFlag(state) {
      return state.flag;
    },
  },

  mutations: {
    passLabelsBorrowed(state, labels) {
      return (state.labelsBorrowed = labels);
    },
    passDataBorrowed(state, data) {
      return (state.dataBorrowed = data);
    },
    passLabelsReturned(state, labels) {
      return (state.labelsReturned = labels);
    },
    passDataReturned(state, data) {
      return (state.dataReturned = data);
    },
    setFlag(state, flag) {
      return (state.flag = flag);
    },
  },

  actions: {
    getDataLabelsBorrowed({ commit }) {
      sort(
        `${DEFAULT_API_URL}/borrowed/books`,
        commit,
        "passDataBorrowed",
        "passLabelsBorrowed"
      );
    },

    getDataLabelsReturned({ commit }) {
      sort(
        `${DEFAULT_API_URL}/returned/books`,
        commit,
        "passDataReturned",
        "passLabelsReturned",
        true
      );
    },

    getAll({ dispatch }) {
      dispatch("getDataLabelsBorrowed");
      dispatch("getDataLabelsReturned");
    },
  },
};

const sort = (url, commit, passData, passLabels, flag) => {
  axios.get(url).then((response) => {
    let labels = [
      "Science and Fiction",
      "Novel",
      "Educational",
      "Crime",
      "Biography",
    ];
    let data = [0, 0, 0, 0, 0];
    for (let x in response.data) {
      for (let i in labels) {
        if (labels[i] === response.data[x].books[0].categories.category) {
          data[i] += response.data[x].copies;
        }
      }
    }
    commit(passLabels, labels);
    commit(passData, data);
    flag ? commit("setFlag", flag) : null;
  });
};
