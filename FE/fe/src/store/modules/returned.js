import axios from "axios";
const { DEFAULT_API_URL } = require("../../../config");

export const returned = {
  namespaced: true,
  actions: {
    addReturn({ dispatch, commit }, info) {
      axios
        .post(`${DEFAULT_API_URL}/returned/books/add`, info)
        .then((response) => {
          commit("showReturn", "", { root: true });
          dispatch("books/throwMessage", response.data.success, { root: true });
        })
        .catch((error) =>
          dispatch("books/throwMessage", error.response.data, { root: true })
        );
    },
  },
};
