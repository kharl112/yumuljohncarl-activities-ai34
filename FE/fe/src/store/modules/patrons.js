import axios from "axios";
const { DEFAULT_API_URL } = require("../../../config");

export const patrons = {
  namespaced: true,
  state: () => ({
    patrons: [],
    message: {},
  }),
  getters: {
    listPatrons(state) {
      return state.patrons;
    },
    showToast(state) {
      return state.message;
    },
  },
  mutations: {
    getList(state, patrons) {
      return (state.patrons = patrons);
    },
    getMessage(state, msg) {
      return (state.message = msg);
    },
  },
  actions: {
    getPatrons({ commit }) {
      axios.get(`${DEFAULT_API_URL}/patrons`).then((response) => {
        commit("getList", response.data);
      });
    },

    addPatron({ dispatch, commit }, patron) {
      axios
        .post(`${DEFAULT_API_URL}/patrons/add`, patron)
        .then((response) => {
          dispatch("getPatrons");
          commit("showAdd", "", { root: true });
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => {
          dispatch("throwMessage", error.response.data);
        });
    },

    editPatron({ dispatch, commit }, patron) {
      axios
        .put(`${DEFAULT_API_URL}/patrons/update`, patron)
        .then((response) => {
          dispatch("getPatrons");
          commit("showAdd", "", { root: true });
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => {
          dispatch("throwMessage", error.response.data);
        });
    },

    deletePatron({ dispatch }, id) {
      axios
        .delete(`${DEFAULT_API_URL}/patrons/delete/id=${id}`)
        .then((response) => {
          dispatch("getPatrons");
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => {
          dispatch("throwMessage", error.response.data);
        });
    },

    throwMessage({ commit }, message) {
      commit("getMessage", message);
      setTimeout(() => {
        commit("getMessage", "");
      }, 5000);
    },
  },
};
