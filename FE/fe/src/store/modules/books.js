import axios from "axios";
const { DEFAULT_API_URL } = require("../../../config");

export const books = {
  namespaced: true,
  state: () => ({
    books: [],
    message: {},
  }),
  getters: {
    listBooks(state) {
      return state.books;
    },
    showToast(state) {
      return state.message;
    },
  },
  mutations: {
    getList(state, books) {
      return (state.books = books);
    },
    getMessage(state, msg) {
      return (state.message = msg);
    },
  },
  actions: {
    getBooks({ commit }) {
      axios.get(`${DEFAULT_API_URL}/books`).then((response) => {
        commit("getList", response.data);
      });
    },

    addBook({ dispatch, commit }, book) {
      axios
        .post(`${DEFAULT_API_URL}/books/add`, book)
        .then((response) => {
          dispatch("getBooks");
          commit("showAdd", "", { root: true });
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => dispatch("throwMessage", error.response.data));
    },

    editBook({ dispatch, commit }, book) {
      axios
        .put(`${DEFAULT_API_URL}/books/update`, book)
        .then((response) => {
          dispatch("getBooks");
          commit("showAdd", "", { root: true });
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => dispatch("throwMessage", error.response.data));
    },

    deleteBook({ dispatch }, id) {
      axios
        .delete(`${DEFAULT_API_URL}/books/delete/id=${id}`)
        .then((response) => {
          dispatch("getBooks");
          dispatch("throwMessage", response.data.success);
        })
        .catch((error) => dispatch("throwMessage", error.response.data));
    },

    throwMessage({ commit }, message) {
      commit("getMessage", message);
      setTimeout(() => {
        commit("getMessage", {});
      }, 5000);
    },
  },
};
