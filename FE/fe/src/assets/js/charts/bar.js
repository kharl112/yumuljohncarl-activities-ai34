const data = (borrowed, returned) => ({
    labels: ["Science and Fiction", "Novel", "Educational", "Crime", "Biography"],
    datasets: [{
            label: "Borrowed",
            backgroundColor: "#005099",
            data: borrowed
        },
        {
            label: "Returned",
            backgroundColor: "#D8610B",
            data: returned
        }

    ]
})

const options = {
    title: {
        text: "Borrowed And Returned Books",
        fontColor: "#D8610B",
        lineHeight: 2,
        fontSize: 13,
        position: "top",
        display: true
    },
    legend: {
        position: 'bottom'
    },
    barValueSpacing: 20,
    scales: {
        xAxes: [{
            gridLines: {
                offsetGridLines: true
            }
        }]
    }
}

module.exports = {
    options,
    data
}