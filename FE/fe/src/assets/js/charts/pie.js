const data = (data, labels) => ({
    labels: labels,
    datasets: [{
        backgroundColor: ["#27AE60", "#2F80ED", "#9B51E0", "#EB5757", "#F2994A"],
        data: data,
        borderWidth: 0
    }]
});

const options = {
    cutoutPercentage: 60,
    rotation: 2 * Math.PI,
    title: {
        display: true,
        position: 'top',
        fontSize: 15,
        text: 'Popular Book Genre',
        fontColor: '#48484D'
    },
    legend: {
        position: 'bottom',
        labels: {
            fontSize: 13,
            padding: 12,
            fontFamily: "'Open Sans', sans-serif"
        }
    }
}

module.exports = {
    data,
    options
}