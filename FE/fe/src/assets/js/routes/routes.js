import dashboard from '../../../components/pages/dashboard/dashboard';
import patrons from '../../../components/pages/patrons/patrons';
import books from '../../../components/pages/books/books';
import settings from '../../../components/pages/settings/settings';
export const routes = [
    {path: '/', component: dashboard},
    {path: '/dashboard', component: dashboard},
    {path: '/patrons', component: patrons},
    {path: '/books', component: books},
    {path: '/settings', component: settings}
]