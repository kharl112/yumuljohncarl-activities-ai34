import Vue from 'vue';
import App from './App.vue';
import { LayoutPlugin } from 'bootstrap-vue';
import { routes } from './assets/js/routes/routes';
import { store } from './store/store';
import 'bootstrap/dist/css/bootstrap.css';
import VueRouter from 'vue-router';
Vue.config.productionTip = false;



Vue.use(LayoutPlugin);
Vue.use(VueRouter);
const router = new VueRouter({
     routes,
     mode: 'history'
})

new Vue({
     store,
     render: h => h(App),
     router
}).$mount('#app')